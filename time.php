
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
      <!-- Card Header - Accordion -->
      <a href="#losttime" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample1">
        <h6 class="m-0 font-weight-bold text-primary">Tabel Lost Time</h6>
      </a>
      <!-- Card Content - Collapse -->
      <div class="collapse show" id="losttime">
        <div class="card-body">
            <div class="row">
              <div class="col-md-12">
                <div class="card mb-4 py-3 border-bottom-danger">
                  <div class="card-body">
                    <!-- tabel start from here -->
                    <div class="table-responsive">
                      <table class="table table-bordered" id="dataTable" width="20%" cellspacing="0">
                        <thead>
                          <tr>
                            <th>Start</th>
                            <th>Time</th>
                            <th>End</th>
                          </tr>
                        </thead>
                        <tbody>
                          <tr>
                            <td>07.30</td>
                            <td>3.0</td>
                            <td>10.30</td>
                          </tr>
                        </tbody>
                      </table>
                    </div>
                    <!-- end of tabel -->
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      