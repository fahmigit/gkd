
<div class="row">
  <div class="col-lg-6">
    <!-- Collapsable Card Example -->
    <div class="card shadow mb-4">
      <!-- Card Header - Accordion -->
      <a href="#collapseCardExample" class="d-block card-header py-3" data-toggle="collapse" role="button" aria-expanded="true" aria-controls="collapseCardExample">
        <h6 class="m-0 font-weight-bold text-primary">Data SPH Part Item</h6>
      </a>
      <!-- Card Content - Collapse -->
      <div class="collapse show" id="collapseCardExample">
        <div class="card-body">
              <div class="table-responsive">
                <table class="table table-bordered" id="dataTable" width="20%" cellspacing="0">
                  <thead>
                    <tr>
                      <th>No</th>
                      <th>Part Name</th>
                      <th>SPH</th>
                    </tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th>No</th>
                      <th>Part Name</th>
                      <th>SPH</th>
                    </tr>
                  </tfoot>
                  <tbody>
                    <?php 
                    $koneksi2 = mysqli_connect("localhost","root","","gkd");

                    //ambil data dr tabel part
                    $result2 = mysqli_query($koneksi, "SELECT * FROM part ");

                    $no = 1;
                    while($row2 = mysqli_fetch_assoc($result2)): ?>
                    <tr>
                      <td><?php echo $no ++ ?></td>
                      <td><?php echo $row2["nama"];?></td>
                      <td><?php echo $row2["sph"];?></td>
                    </tr>
                  <?php endwhile; ?>
                  </tbody>
                </table>
              </div>
            <!-- end of table -->
        </div>
      </div>
    </div>
  </div>
</div>